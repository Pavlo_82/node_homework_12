const connection = require("./connection");
const args = process.argv.slice(2);

const params = args.reduce((acc, arg) => {
  const [key, value] = arg.split('=');
  acc[key.replace('--', '')] = value;
return acc
}, {});
const {table, field, searchParams} = params
connection.query(
  `SELECT *
  FROM ${table?table:'video'}
  WHERE ${field} LIKE '%${searchParams}%';`,
  (error, results) => {
    if (error) {
      throw error;
    }
    console.log(results.rows);
  }
);

connection.end();