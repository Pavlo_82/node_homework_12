const connection = require("./connection");
const args = process.argv.slice(2);

const params = args.reduce((acc, arg) => {
  const [key, value] = arg.split('=');
  acc[key.replace('--', '')] = value;
return acc
}, {});

const {table, ...rest} = params

let createQuery = `CREATE TABLE ${table ? table : "videos"} ( ID SERIAL PRIMARY KEY,`;

for(let key in rest ) {
  createQuery += ` ${key} ${rest[key]},`
}
createQuery+=` created_date Date`
createQuery += ');'
console.log(createQuery)
connection.query(
  createQuery,
  (error, results) => {
    if (error) {
      throw error;
    }
    console.log(results);
  }
);
connection.end();