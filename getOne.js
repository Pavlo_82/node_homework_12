const connection = require("./connection");
const args = process.argv.slice(2);

const params = args.reduce((acc, arg) => {
  const [key, value] = arg.split('=');
  acc[key.replace('--', '')] = value;
return acc
}, {});
const { table, query, queryText, ...rest } = params;


let findQuery = `SELECT `;

if (Object.keys(rest).length === 0) {
  findQuery += '*';
} else {
  findQuery += Object.keys(rest).join(', ');
}

findQuery += ` FROM ${table?table:'videos'} WHERE ${query}='${queryText}'`;

connection.query(
  findQuery,
  (error, results) => {
    if (error) {
      throw error;
    }
    console.log(results.rows);
  }
);

connection.end();