const connection = require("./connection");
const args = process.argv.slice(2);

const params = args.reduce((acc, arg) => {
  const [key, value] = arg.split('=');
  acc[key.replace('--', '')] = value;
return acc
}, {});

const { id, table, ...rest  } = params;
if (!id ) {
  console.log('Please provide either --id, to specify the record to find.');
  process.exit(1);
}

let findQuery = `SELECT `;

if (Object.keys(rest).length === 0) {
  findQuery += "*";
} else {
  findQuery += Object.keys(rest).join(", ");
}

findQuery += ` FROM ${table ? table : "videos"} WHERE ID = ${+id};`;
console.log(findQuery);
connection.query(
  findQuery,
  (error, results) => {
    if (error) {
      throw error;
    }
    console.log(results.rows);
  }
);

connection.end();