const connection = require("./connection");
const args = process.argv.slice(2);

const params = args.reduce((acc, arg) => {
  const [key, value] = arg.split('=');
  acc[key.replace('--', '')] = value;
return acc
}, {});
const {table, top} = params
connection.query(
  `SELECT category, SUM(views) AS total_views
  FROM ${table?table:'videos'}
  GROUP BY category
  ORDER BY total_views DESC
  LIMIT ${top};`,
  (error, results) => {
    if (error) {
      throw error;
    }
    console.log(results.rows);
  }
);

connection.end();