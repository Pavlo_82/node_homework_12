const connection = require("./connection");
const args = process.argv.slice(2);

const params = args.reduce((acc, arg) => {
  const [key, value] = arg.split('=');
  acc[key.replace('--', '')] = value;
return acc
}, {});
const {table} = params

connection.query(`DROP TABLE ${table?table:'videos'};`, (error, results) => {
  if (error) {
    throw error;
  }
  console.log(results);
});
connection.end();