node createTable.js --title='VARCHAR(60)' --views='FLOAT4' --category='VARCHAR(60)'

node insert.js --insertData='[Video 1, 4.5, fanny, Video 2, 5.0, triller, Video 3, 4.8, dramma, Video 4, 3.6, dramma, Video 5, 3.2, fanny, Video 6, 4.0, triller, Video 7, 2.2, dramma, Video 8, 4.7, comedy, Video 9, 4.65, triller, Video 10, 5.0, fanny]' --table=videos --customColumn1='title' --customColumn2='views' --customColumn3='category'

node delete.js --table=videos --query='id' --textQuery='7'

node getAll.js --table=videos --title=title

node getById.js --table=videos --id=5 --title=title --category=category

node find.js --table=videos --field=title --searchParams=deo 9

node group.js --table=videos --field=title --searchParams=views --orderBy=DESC

node top.js --table=videos --top=5

node getOne.js --table=videos --query='title' --queryText='Video 9' --title=title --category=category

node update.js --table=videos --query='title' --textQuery='Video 1' --title='hcnjmkmnhjmnf' --text='ggffdgfggtff'

node dropTable.js
