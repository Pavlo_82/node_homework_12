const connection = require("./connection");
const page = 0
const size = 10
connection.query(
  `SELECT * FROM videos LIMIT ${size} OFFSET ${page};`,
  (error, results) => {
    if (error) {
      throw error;
    }
    console.log(results.rows);
  }
);

connection.end();