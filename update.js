const connection = require("./connection");

const args = process.argv.slice(2);

const params = args.reduce((acc, arg) => {
  const [key, value] = arg.split("=");
  acc[key.replace("--", "")] = value;
  return acc;
}, {});

const { id, table, ...rest } = params;

let updateQuery = "";

updateQuery = `UPDATE ${table ? table : "videos"} SET `;
const updateValues = [];

for (const key in rest) {
  updateValues.push(`${key} = '${rest[key]}'`);
}

updateQuery += updateValues.join(", ");

const { title, text } = rest;

if (!id && !title && !text) {
  console.log(
    "Please provide either --id, --title, or --text to specify the record to update."
  );
  process.exit(1);
}
if (id) {
  updateQuery += `ID = ${+id}`;
} else if (title) {
  updateQuery += `title = '${title}'`;
} else if (content) {
  updateQuery += `text = '${text}'`;
}

updateQuery += ` WHERE ID = ${+id};`;

console.log("SQL Query:", updateQuery);

connection.query(updateQuery, (error, results) => {
  if (error) {
    throw error;
  }
  console.log(results);
});

connection.end();
