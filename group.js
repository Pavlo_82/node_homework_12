const connection = require("./connection");
const args = process.argv.slice(2);

const params = args.reduce((acc, arg) => {
  const [key, value] = arg.split('=');
  acc[key.replace('--', '')] = value;
  return acc;
}, {});

const { table, field, searchParams, orderBy } = params;
console.log(`SELECT category, SUM(${searchParams}) AS total_views
FROM ${table ? table : 'videos'}
GROUP BY category, ${field}
ORDER BY total_views ${orderBy};`);
connection.query(
  `SELECT category, SUM(${searchParams}) AS total_views
  FROM ${table ? table : 'videos'}
  GROUP BY category, ${field}
  ORDER BY total_views ${orderBy};`,
  (error, results) => {
    if (error) {
      throw error;
    }
    console.log(results.rows);
  }
);

connection.end();